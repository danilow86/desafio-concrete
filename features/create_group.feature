Feature: Create Group

	Scenario: Creating a group
		Given The Whatsapp app is opened
		When I select new group
		Then the group is created
	
	Scenario: Creating Broadcast List
		Given The Whatsapp app is opened
		When I select Broadcast list 
		Then The BroadCast List is created
	
	