Feature: Starting Chat

	Scenario: Starting a conversation
		Given The Whatsapp app is opened
		When I select new chat
		And tap on contact to start
		Then the chat is opened 
	
	Scenario: Starting a new conversation
		Given The Whatsapp app is opened
		When I select new chat
		Then the chat is opened 	